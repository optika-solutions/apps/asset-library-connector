from extra_api import get_assets, update_assets
import pprint


def akumen(data, **kwargs):
    """
        - Input: data [file] (csv)
    """
    
    # we expect three columns in our input: asset, attribute, value
    if 'asset' not in data.columns or 'attribute' not in data.columns or 'value' not in data.columns:
        raise Exception('Input data table requires three columnes: `asset`, `attribute` and `value`.')
    
    # pull the list of current assets
    assets = get_assets()

    updated_assets = {}
    
    # reform it into tuple keys
    for row in data.itertuples():
        if row.asset in updated_assets:
            asset = updated_assets[row.asset]
        else:
            asset = assets[row.asset]
            asset['attributes'] = {}
        
        asset['attributes'][row.attribute] = row.value
        updated_assets[row.asset] = asset
    
    assets = update_assets(updated_assets)
    
    return {}