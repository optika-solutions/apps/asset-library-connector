import os
import requests
from akumen_api import API_KEY, AKUMEN_URL, AKUMEN_API_URL


def call_model(model_name, **kwargs):
    url = os.path.join(AKUMEN_API_URL, 'execute', model_name)
    headers = {'Authorization': API_KEY}
    
    resp = requests.post(url, headers=headers, data=kwargs)
    resp.raise_for_status()
    
    # parse returns
    result = resp.json()
    
    if result.get('status', 'Error') == 'Error':
        raise Exception('Model call failed: ' + result.get('error_log', 'Unknown error.'))
    
    retval = {}
    for name, data in result.get('simple_outputs', {}).items():
        if name == 'InternalOutput':
            for item in data:
                if item.get('FloatValue'):
                    retval[item.get('Name')] = float(item.get('FloatValue'))
                elif item.get('StringValue'):
                    try:
                        val = json.loads(item.get('StringValue'))
                        retval[item.get('Name')] = val
                    except Exception:
                        retval[item.get('Name')] = item.get('StringValue')
        else:
            retval[name] = data
    
    for name, data in result.get('outputs', {}).items():
        retval[name] = data
    
    return retval


def get_assets(**kwargs):
    url = os.path.join(AKUMEN_URL, 'api/v2', 'assets')
    headers = {'Authorization': API_KEY}
    
    assets = {}
    page = 0
    page_size = 20
    while True:
        resp = requests.get(url, headers=headers, data=kwargs, params={'page': page, 'pageSize': page_size})
        resp.raise_for_status()
        
        data = resp.json()
        assets.update(data)
        
        if len(data) < page_size:
            break
        
        page += 1
    
    return assets

def update_assets(assets, **kwargs):
    url = os.path.join(AKUMEN_URL, 'api/v2', 'assets')
    headers = {'Authorization': API_KEY}
    
    resp = requests.put(url, headers=headers, json=assets)
    resp.raise_for_status()
    
    return resp.json()